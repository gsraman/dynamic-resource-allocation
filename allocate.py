import random
import sys
from bottle import route,run

#Initialization
@route('/<no_of_users:int>/<no_of_servers:int>/<ram_size:int>')
def function(no_of_users,no_of_servers,ram_size):
    max_size_of_process = 0.75 * ram_size
    user_process_dict = {}
    allocate_dict = {}
    server_status_dict = {}
    server_mem_left_dict = {}

    for i in range(1,no_of_users+1):
        user_process_dict[i] =   random.randint(1,max_size_of_process) #{1: 179, 2: 337, 3: 87, 4: 31, 5: 223}
        allocate_dict[i] = False

    for i in range(1,no_of_servers+1):
        server_status_dict[i] = False
        server_mem_left_dict[i] = ram_size - 0.25*ram_size


    # print user_process_dict,allocate_dict,server_status_dict,server_mem_left_dict
    current_server_number = 1
    print "User process dict",user_process_dict



    while False in allocate_dict.values() and current_server_number <= no_of_servers:
            for request_process in sorted(user_process_dict, key=user_process_dict.get,reverse=True):
                if allocate_dict[request_process] == False:
                    if server_mem_left_dict[current_server_number] >= user_process_dict[request_process]:
                        if server_status_dict[current_server_number] == False:
                            server_status_dict[current_server_number] = True
                        allocate_dict[request_process] = current_server_number
                        server_mem_left_dict[current_server_number] -= user_process_dict[request_process]
            current_server_number += 1

    for status in server_status_dict:
        if server_status_dict[status]==True:
            server_status_dict[status]="On"
        else:
            server_status_dict[status]="Off"

    for server in server_mem_left_dict:
        server_mem_left_dict[server] += 0.25*ram_size

    if False in allocate_dict.values():
        print "All process not allocated"
        print "allocate_dict ",allocate_dict
        print "server_status_dict",server_status_dict
        print "server_mem_left_dict",server_mem_left_dict
        result = { "result":"failure","allocation":allocate_dict,"server_states":server_status_dict,"server_memory_left":server_mem_left_dict}
        return result
    else:
        print "All process allocated"
        print "allocate_dict ",allocate_dict
        print "server_status_dict",server_status_dict
        print "server_mem_left_dict",server_mem_left_dict
        result = { "result":"success","allocation":allocate_dict,"server_states":server_status_dict,"server_memory_left":server_mem_left_dict}
        return result

run(host='0.0.0.0', port=3000, debug=True)

# for requested_user in sorted(user_process_dict, key=user_process_dict.get,reverse=True):
#      mem_available_current_server = server_mem_left_dict[current_server_number]
#    run(host='0.0.0.0', port=80)  if user_process_dict[requested_user] <= mem_available_current_server:
#          if server_status_dict[current_server_number] = False:
#              server_status_dict[current_server_number] = True
#              server_mem_left_dict[current_server_number]-= user_process_dict[requested_user]
#

    #     while allocate_dict[max_value_unallocated_key] == False and server_mem_left_dict[current_server_number] >= max_value_unallocated:
    #         allocate_dict[max_value_unallocated_key] = current_server_number
    #         server_mem_left_dict[current_server_number] -= user_process_dict[max_value_unallocated_key]
    #         if server_status_dict[current_server_number]==False:
    #             server_status_dict[current_server_number] = True
    #         for key in user_process_dict:
    #             if (allocate_dict[key]==False) and (max_value_unallocated<user_process_dict[key]):
    #                 max_value_unallocated = user_process_dict[key]
    #                 max_value_unallocated_key = key
    #     if server_mem_left_dict[current_server_number] < max_value_unallocated:
    #         current_server_number += 1
    #         if current_server_number > no_of_servers:
    #             print "Not enough servers"
    #         allocate_dict[max_value_unallocated_key] = current_server_number
    #         if max_value_unallocated != 0:
    #             server_mem_left_dict[current_server_number] -= user_process_dict[max_value_unallocated_key]
    #         if server_status_dict[current_server_number]==False:
    #             server_status_dict[current_server_number] = True
    #
